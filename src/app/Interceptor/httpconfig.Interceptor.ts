import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Injectable()

export class HttpConfigInterceptor implements HttpInterceptor {
    @BlockUI() blockUI: NgBlockUI;
    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.blockUI.start("Loading...");
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    this.blockUI.stop();
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                this.blockUI.stop();
                return throwError(error);
            }));
    }
}
