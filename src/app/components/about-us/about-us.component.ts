import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from 'src/app/service/data.service';

import * as _ from 'underscore';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import AOS from 'aos';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss'],
  providers: [NgbCarouselConfig]
})
export class AboutUsComponent implements OnInit {
  public aboutUsData: any = {};
  showNavigationArrows = false;
  showNavigationIndicators = false;

  constructor(private _sanitizer: DomSanitizer, config: NgbCarouselConfig, private dataService: DataService) {
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
    AOS.init({
      duration: 500,
      delay: 500,
      once: true
    });
  }

  // {
  //   "row1": [
  //     {
  //       "type": "text",
  //       "header": "OUR STORY",
  //       "data": "<p>'Lepos' is a word derived from the Latin origin meaning pleasantness, grace and charm. True to its meaning, our Brand Lepos is made with an endeavour to add that European element of grace and charm to your homes. Our homes are a reflection of us- our choices could be timeless or contemporary; the bouquet of products at Lepos have it all to match the spirits of your homes. <br><br> Each sanitaryware piece at Lepos is specially curated to not just suit the needs and requirements but also to add the aesthetic touch to your homes. Get your range of elegant products with the perfect finishing in Faucets, Vessels and Water closets at your doorstep. Whether you prefer:  <br>The Classics- glossy whites/ blacks OR<br>The Sublime matt finishes- beiges/blacks OR<br>The Appealing coffee brown/cemento grey OR<br>The Demure soft blues OR<br>The Indian love for peacock hues OR<br>The Royal gold and Rose gold products; you will find it all within our wide range of sanitaryware. <br><br>It is truly Designer luxury made available for everyone to enrich your lifestyle and homes.</p><br>",
  //       "button": {
  //         "text": "Go Somewhere",
  //         "link": "/contact"
  //       }
  //     },
  //     {
  //       "type": "video",
  //       "data": "https://www.youtube.com/embed/1ozGKlOzEVc"
  //     }
  //   ],
  //   "row2": [
  //     {
  //       "type": "carousel",
  //       "data": [
  //         {
  //           "image": "https://picsum.photos/id/1055/900/500",
  //           "text": "Image 3"
  //         }
  //       ]
  //     },
  //     {
  //       "type": "text",
  //       "header": "OUR STORY",
  //       "data": "In 2006, Lepos started providing more than just basic sanitary ware and bathroom fixtures in the Indian market by actively seeking out exclusive and innovative products. <br><br>Our founders realized the need for exclusive, fashion forward products in the world of bath design. Thus began the progression of traveling the world to visit factories for the latest designs, quality materials and workmanship.<br><br>Within a decade, the brand had become a trusted name and expanded to over 300 dealers with presence in 52 cities and towns across India."
  //     }
  //   ],
  //   "row3": [
  //     {
  //       "type": "text",
  //       "header": "10-YEAR WARRANTY",
  //       "data": "We are confident that our products will give you many years of trouble free operation. However, for peace of mind, all our products are covered by warranties upto 10-Years. <br><br>Our Team is passionate about delivering Exceptional Customer Service. We have a dedicated Toll-Free Customer Helpline backed by Service-Centres across the country.<br><br>For complete Warranty & Customer Care details, use the following link:",
  //       "button": {
  //         "text": "Go Somewhere",
  //         "link": "/contact"
  //       }
  //     },
  //     {
  //       "type": "carousel",
  //       "data": [
  //         {
  //           "image": "https://picsum.photos/id/194/900/500",
  //           "text": "Image 1"
  //         },
  //         {
  //           "image": "https://picsum.photos/id/368/900/500",
  //           "text": "Image 2"
  //         },
  //         {
  //           "image": "https://picsum.photos/id/1055/900/500",
  //           "text": "Image 3"
  //         }
  //       ]
  //     }
  //   ]
  // }

  ngOnInit(): void {
    this.dataService.getConfigById({ configID: "5" }).subscribe((data: any) => {
      this.aboutUsData = JSON.parse(data.data.data);
      _.each(this.aboutUsData, (value, key) => {
        _.each(value, (data) => {
          if (data.type == "video") {
            data.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(data.data);
          }
        })
      });
    });
  }

}
