import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms'; 

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  public contactForm: FormGroup;
  public profession: any[];
  public states: any[];

  constructor(private formbuilder: FormBuilder) {
    this.profession = ["Private Client", "Interior Designer", "Architect", "Other"];
    this.states = [{"key":"AN","name":"Andaman and Nicobar Islands"},{"key":"AP","name":"Andhra Pradesh"},{"key":"AR","name":"Arunachal Pradesh"},{"key":"AS","name":"Assam"},{"key":"BR","name":"Bihar"},{"key":"CG","name":"Chandigarh"},{"key":"CH","name":"Chhattisgarh"},{"key":"DH","name":"Dadra and Nagar Haveli"},{"key":"DD","name":"Daman and Diu"},{"key":"DL","name":"Delhi"},{"key":"GA","name":"Goa"},{"key":"GJ","name":"Gujarat"},{"key":"HR","name":"Haryana"},{"key":"HP","name":"Himachal Pradesh"},{"key":"JK","name":"Jammu and Kashmir"},{"key":"JH","name":"Jharkhand"},{"key":"KA","name":"Karnataka"},{"key":"KL","name":"Kerala"},{"key":"LD","name":"Lakshadweep"},{"key":"MP","name":"Madhya Pradesh"},{"key":"MH","name":"Maharashtra"},{"key":"MN","name":"Manipur"},{"key":"ML","name":"Meghalaya"},{"key":"MZ","name":"Mizoram"},{"key":"NL","name":"Nagaland"},{"key":"OR","name":"Odisha"},{"key":"PY","name":"Puducherry"},{"key":"PB","name":"Punjab"},{"key":"RJ","name":"Rajasthan"},{"key":"SK","name":"Sikkim"},{"key":"TN","name":"Tamil Nadu"},{"key":"TS","name":"Telangana"},{"key":"TR","name":"Tripura"},{"key":"UK","name":"Uttar Pradesh"},{"key":"UP","name":"Uttarakhand"},{"key":"WB","name":"West Bengal"}];
    this.contactForm = this.formbuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', [Validators.required, Validators.minLength(15)]],
      profession: ['', Validators.required],
      company: [''],
      state: [''],
      city: ['', Validators.required],
      area: [''],
      phone: ['', [Validators.required, Validators.minLength(10)]]
    });
  }

  ngOnInit(): void { }

  onSubmit(form: FormGroup) {
    console.log('Valid?', form.valid, form);
  }

  changefield(e){
    console.log(e);
    this.contactForm.setValue(e.target.value, {
      onlySelf: true
    });
  }

}
