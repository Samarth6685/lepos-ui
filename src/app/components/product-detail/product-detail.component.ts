import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DataService } from 'src/app/service/data.service';

import Swal from "sweetalert2/dist/sweetalert2.js";
import * as slick from 'src/assets/scripts/slick-1.8.1/slick/slick.min.js';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery-9';
declare var $: any;

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit, AfterViewInit {

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  public ProductData: any;

  constructor(private _Activatedroute: ActivatedRoute, private dataService: DataService) {
    this.galleryOptions = [
      {
        thumbnailsColumns: 4,
        lazyLoading: true,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];

    this.galleryImages = [];
  }

  ngOnInit(): void {
    const id = this._Activatedroute.snapshot.paramMap.get("id");
    this.getProductDetails(id);
  }

  ngAfterViewInit() {

  }

  getProductDetails(id: string) {
    var payload = { "productID": id };
    this.dataService.getProductDetails(payload).subscribe((data) => {
      if (data.details) {
        this.ProductData = data.details;
        this.dataService.getProductimages(payload).subscribe((data) => {
          if (data.status) {
            if(data.images.length == 1){
              this.galleryOptions[0].thumbnails = false;
              this.galleryOptions[0].imageArrows = false;
            }
            data.images.forEach(element => {
              this.galleryImages.push({
                small: 'http://lepos.in/lepos_products_image/'+element.path,
                medium: 'http://lepos.in/lepos_products_image/'+element.path,
                big: 'http://lepos.in/lepos_products_image/'+element.path
              })
            });
            this.ProductData.imgarr = data.images;
            this.initSlider();
            console.log(this.ProductData);
          }
        });
      }
    })

  }
  initSlider() {
    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: true,
      centerMode: true,
      focusOnSelect: true
    });
  }
  checkifstrint(strint){
    if(typeof(strint) === 'number'){
      return false
    } else {
      return true
    }
  }

}
