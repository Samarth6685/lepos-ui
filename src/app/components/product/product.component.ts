import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router, Event, NavigationEnd } from '@angular/router';

import { DataService } from 'src/app/service/data.service';

import Swal from "sweetalert2/dist/sweetalert2.js";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import AOS from 'aos';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, AfterViewInit {
  public catData: any;
  public products: any;
  public subscription: any;
  public ProductData: any;

  constructor(
    private _Activatedroute: ActivatedRoute,
    private dataService: DataService,
    private router: Router,
    private modalService: NgbModal) {
      AOS.init({
        duration: 500,
        delay: 500,
        once: true
      });
    }

  ngOnInit(): void { }

  ngAfterViewInit(){
    var id = this._Activatedroute.snapshot.paramMap.get("id");
    var subCatID, catID;
    subCatID = id.split("/")[0];
    catID = id.split("/")[1];
    this.getProductsData(subCatID, catID);
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        console.log(event);
        var routeArr = event.url.split('/');
        console.log(routeArr);
        if(routeArr[1] == "products"){
          id = this._Activatedroute.snapshot.paramMap.get("id");
          subCatID = id.split("/")[0];
          catID = id.split("/")[1];
          this.getProductsData(subCatID, catID);
        }
      }
    });
  }

  ngOndestroy() {
    
  }

  getProductsData(subCatID, catID) {
    var payload = { 'subCatID': subCatID, 'catID': catID };
    this.dataService.getProductBySubcatid(payload).subscribe((data) => {
      if (data.status == false) {
        Swal.fire("Error", "Something went wrong. Please try later", "error");
      } else {
        this.catData = data.subCategory;
        this.products = data.products;
      }
    })
  }

  addtoselection(product) {
    console.log(product);
  }

  viewProductDetails(product, content) {
    console.log(product);
    this.ProductData = product;
    this.modalService.open(content, { size: 'lg', windowClass: 'main-model'}).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  checkifstrint(strint) {
    var ddd = parseFloat(strint);
    //@ts-ignore
    if (ddd == NaN) {
      return true;
    } else if (ddd != NaN && typeof (ddd) == 'number') {
      return false;
    }
  }
}
