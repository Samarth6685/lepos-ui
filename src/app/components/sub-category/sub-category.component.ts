import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router, Event, NavigationEnd } from '@angular/router';

import { DataService } from 'src/app/service/data.service';

import Swal from "sweetalert2/dist/sweetalert2.js";

@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.scss']
})
export class SubCategoryComponent implements OnInit, AfterViewInit {
  public SubCatData: any;
  public catData: any;
  public catid: any;

  constructor(private _Activatedroute: ActivatedRoute, private dataService: DataService, private router: Router) { }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(){
    var id = this._Activatedroute.snapshot.paramMap.get("id");
    this.catid = id;
    this.getSubCategoryData(id);
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        console.log(event);
        var routeArr = event.url.split('/');
        console.log(routeArr);
        if(routeArr[1] == "sub-category"){
          id = this._Activatedroute.snapshot.paramMap.get("id");
          this.getSubCategoryData(id);
        }
      }
    });
  }

  getSubCategoryData(id) {
    var payload = { 'catID': id };
    this.dataService.getSubCategoryById(payload).subscribe((data) => {
      if (data.status == false) {
        Swal.fire("Error", "Something went wrong. Please try later", "error");
      } else {
        this.SubCatData = data.subCategory;
        this.catData = data.category;
      }
    })
  }

}
