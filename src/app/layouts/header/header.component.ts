import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';

declare var $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
  public headerObj: any = [];
  public socialIcons: any = [];
  public mobileNo: string = "";
  public location: any = "";

  constructor(private modalService: NgbModal, private dataService: DataService, public router: Router) { }

  ngOnInit(): void {
    this.dataService.getConfigById({ configID: "1" }).subscribe((data: any) => {
      this.headerObj = JSON.parse(data.data.data);
    });
    this.dataService.getConfigById({ configID: "2" }).subscribe((data: any) => {
      this.socialIcons = JSON.parse(data.data.data);
    });
    this.dataService.getConfigById({ configID: "3" }).subscribe((data: any) => {
      this.mobileNo = data.data.data;
    });
    this.dataService.getConfigById({ configID: "4" }).subscribe((data: any) => {
      this.location = data.data.data;
    });
  }

  ngAfterViewInit() {
    var width = $(window).width();
    if (width > 500) {
      $(window).scroll(function () {
        if ($(window).scrollTop() > 700) {
          $('.secondary-header').css({ display: 'block' });
        } else {
          $('.secondary-header').css({ display: 'none' });
        }
      });
    }
  }

  findObjLength(obj) {
    var count = Object.keys(obj).length;
    return count;
  }
  
  hidden(e){
    $("#"+e).prev(".card-header").find(".fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
  }

  shown(e){
    $("#"+e).prev(".card-header").find(".fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', windowClass: 'header-model' }).result.then((result) => {
    }, (reason) => {
    });
  }

}
