import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from '../service/data.service';

import Swal from "sweetalert2/dist/sweetalert2.js";
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {

  constructor(private dataService: DataService, public router: Router, public config: NgbCarouselConfig) {
    config.showNavigationIndicators = false;
  }

  ngOnInit(): void { }

  ngAfterViewInit() { }

  categoryClick(id) {
    var payload = { 'catID': id };
    // this.dataService.getSubCategoryById(payload).subscribe((data) => {
    //   if (data.status == false) {
    //     Swal.fire("Error", "Something went wrong. Please try later", "error");
    //   } else {
    //     const sendData = { data: data.data };
    //     this.router.navigateByUrl('/sub-category', { state: sendData })
    //   }
    // })
  }

}
