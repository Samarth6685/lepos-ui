import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  // Http Options
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(public http: HttpClient) { }

  signup(data): Observable<any> {
    return this.http.post<any>(`${environment.base_url}/user_reg`, data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  getSubCategoryById(params){
    return this.http.post<any>(`${environment.base_url}/get_subCategories_by_catId`, params)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getProductBySubcatid(params){
    return this.http.post<any>(`${environment.base_url}/get_product_by_subCatId`, params)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getProductDetails(params){
    return this.http.post<any>(`${environment.base_url}/get_product_details`, params)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getProductimages(params){
    return this.http.post<any>(`${environment.base_url}/get_product_images`, params)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getConfigById(params){
    return this.http.post<any>(`${environment.base_url}/get_config_by_id`, params)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  // Error handling 
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
